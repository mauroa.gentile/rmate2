# RMate2

Curso breve del lenguaje R aplicado a los contenidos de la materia Matemática 2

Licenciatura en biología - UNCo - Bariloche

## Autor

Mauro Gentile  

## Versión

2023  

## Bibliografía

2017 - Universidad Nacional del Comahue - Centro Regional Universitario Bariloche - de Torres Curth, M  y Koennecke, A.  Matrices  y  ecuaciones  diferencialesordinarias con R. Operaciones, cálculos y aplicaciones. ISSN –   0325 – 6308

2014 - O'Reilly Media, Incorporated -  Grolemund, G - Hands-on Programming with R. https://rstudio-education.github.io/hopr/

## info
- [ ] [R](https://www.r-project.org/)
- [ ] [RStudio](https://www.rstudio.com/)

## Programa

1. Introduccion a R  
    1.1. ¿Que es y para que sirve R?  
    1.2. RStudio: una interfaz de trabajo con R  
    1.3. Algunos detalles iniciales  
    1.4. R como calculadora  
2. Ingresar datos a R  
    2.1. Ingresando matrices  
    2.2. Asignar nombres a las filas y columnas de una matriz  
    2.3. Extraer partes de una matriz  
    2.4. Listas y hojas de datos  
        2.4.1. Listas  
        2.4.2. Hojas de datos (data frames)  
    2.5.   Leer datos en R  
        2.5.1. Corregir los datos importados  
        2.5.2. foo  
3. Matrices  
    3.1. Operaciones con matrices  
        3.1.1. Sumar matrices  
        3.1.2. Multiplicar matrices por un número real  
        3.1.3. Productos de matrices  
        3.1.4. Potencias  
        3.1.5. Trasposición de matrices  
    3.2. Otras cosas  ́utiles  
    3.3. Determinante, inversa y rango  
        3.3.1. Determinante  
        3.3.2. Inversa  
        3.3.3. Rango  
    3.4. Cálculo de autovalores y autovectores
    3.5. Matrices elementales  
    3.6. Sistemas de ecuaciones  
4. Condiciones, Bucles y Funciones  
    4.1. Operadores Lógicos  
    4.2. Condicionales  
        4.2.1. El comando *if*  
        4.2.2. El comando *if/else*  
        4.2.3. Condiciones múltiples  
    4.3. Bucles  
        4.3.1. El comando *for*  
        4.3.2. El comando *while*  
    4.4. Funciones  
    4.5. La familia de funciones *apply*  
5. Graficar con R  
    5.1. Gráficos básicos en R  
        5.1.1. Comandos de alto nivel  
        5.1.2. Comandos de bajo nivel  
    5.2. El comando ``curve()``  
    5.3. El comando ``par()``  
    5.4. El comando ``layout()``  
6. Los paquetes ``Popbio`` y ``Rramas``  
    6.1. Modelos discretos. Poblaciones estructuradas  
    6.2. Modelos determinísiticos y estocásticos  
        6.2.1. El caso determinístico  
        6.2.2. Qué información nos da la matriz de proyección  
        6.2.3. El caso estocástico   
        6.2.4. Qué información podemos obtener en el caso estocástico  
    6.3. El paquete ``PopBio``  
        6.3.1. El caso determinístico con ``PopBio``  
        6.3.2. Análisis básico y proyección poblacional con ``PopBio``  
        6.3.3. El caso estocástico con ``PopBio``  
    6.4.   El paquete ``Rramas``  
        6.4.1. El caso determinístico con ``Rramas``  
        6.4.2. Análisis básico y proyección poblacional con ``Rramas``  
        6.4.3. El caso estocástico con ``Rramas``  
7. Ecuaciones Diferenciales. El paquete ``deSolve``  
    7.1. Ecuaciones Diferenciales Ordinarias  
    7.2. Modelos con retardo  
    7.3. Sistemas de Ecuaciones Diferenciales Ordinarias Lineales  
    7.4. Análisis cualitativo de sistemas no lineales  
        7.4.1. Funciones clave  
        7.4.2. Sistemas dinámicos autónomos unidimensionales  
        7.4.3. Sistemas dinámicos autónomos bidimensionales  
    7.5. Sistemas de Ecuaciones Diferenciales Ordinarias Lineales con retardo  
